# Workshop02 - Instalación de herramientas LAMP (Apache, MySQL, PHP)

## Iniciar Máquina virtual

Primero debemos de levantar la MV ingresando mediante cygwin al directorio donde tenemos el archivo Vagrantfile para posteriormente ejecutar el siguiente comando:

```bash
vagrant up      
```

## Conección mediante SSH

Procedemos a conectarnos a la MV mediante ssh":

```bash
vagrant ssh      
```
## Cambiar el hostnamede la máquina virtual

Cambiamos el hostname mediante el siguiente comando para posteriormente salir de la MV y volver a ingrezar para ver reflejado el cambio:

```bash
sudo hostnamectl set-hostname webserver 
exit
vagrant ssh
```
 
## Actualizar el  hostname en el archivo hosts

Aca debemos de terminar de completar el cambio del nombre de la máquina la ruta /etc/hosts

```bash
sudo nano /etc/hosts
```
Cuando el editor de texto esté abierto, cambiamos el nombre, presionamos Ctrl + O + enter para guardar y luego Ctrl + x para salir del editor 


## Actualizar la lista de paquetes elegibles

Primero debemos de actualizar los paquetes elegibles en la MV antes de poder instalarlos, todo mediante el sig comando:

```bash
sudo apt-get update
```

## Instalar paquetes

Ahora si podemos proceder a instalar los paquetes necesarios para nuestro proyecto:

```bash
sudo apt-get install vim vim-nox curl apache2 mariadb-server mariadb-client php8.2 php8.2-bcmath php8.2-curl php8.2-mbstring php8.2-mysql php8.2-xml php8.2-mcrypt
```

## Comprobación de ip

En la máquina anfitriona podemos proceder a validar que la ip definida en el Vagrantfile se esté comunicando correctamente:

```bash
ping 192.168.56.10
```

## Editar archivo `hosts` en Windows

Mediante el cmd ejecutado como administrador ingresamos en la sig ruta: `c:\Windows\System32\drivers\etc` y procedemos a editar el archivo con la siguiente linea de comandos:

```bash
cd \
cd Windows\System32\drivers\etc
notepad hosts
```

Agrgamos 192.168.56.10 jorge.isw811.xyz guardo el archivo y hago ping en la máquina local

## Verificamos el sitio

Desde la máquina anfitriona visitamos la URL `http://jorge.isw811.xyz` para validar el progreso

## Habilitar los módulos necesarios para soportar hosts virtuales 

ejecutando este comando en la máquina virtual:

```bash
sudo a2enmod vhost_alias rewrite ssl
sudo systemctl restart apache2
```

## Crear carpeta de sitios

Crear un folder local y lo sincronizamos contra la ruta `/home/vagrant/sites` de la máquina virtual

```bash
46 # config.vm.synced_folder " ../data" , "/vagrant_data"
47 config.vm.synced_folder "sites/" , "/home/vagrant/sites" ,
owner: "www-data" , group: "www-data"
```

## Reiniciar máquina

Debemos reiniciarla para que los cambios se vean reflejados

```bash
exit
vagrant halt
vagant up
vagrant ssh
```

## Crear Conf para el sitio 

Por cada sitio que pensemos hospedar en el servidor web debemos de crear un archivo .conf, por lo que procedemos a crear una carpeta que los contenga:

```bash
mkdir confs
cd confs
touch jorge.isw811.xyz.conf
code jorge.isw811.xyz.conf
```

## Creamos la configuración en el archivo: 

```bash
<VirtualHost *:80>
     ServerAdmin webmaster@jorge.isw811.xyz
     ServerName jorge.isw811.xyz
   
     DirectoryIndex index.php index.html
     DocumentRoot /home/vagrant/sites/jorge.isw811.xyz
   
     <Directory /home/vagrant/sites/jorge.isw811.xyz>
       DirectoryIndex index.php index.html
      AllowOverride All
      Require all granted
    </Directory>
  
    ErrorLog ${APACHE_LOG_DIR}/jorge.isw811.xyz.error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/jorge.isw811.xyz.access.log combined
  </VirtualHost>
```

## Copiar archivo

Desde la máquina virtual vamos a copiar el archivo .conf a la ruta de
sitios disponibles de Apache2.

```bash
sudo cp /vagrant/confs/jorge.isw811.xyz.conf
/etc/apache2/sites-available
```

## Verificación de Apache

Comprobar que en los archivos de configuración recién agregados no se haya introducido ningún error

```bash
sudo apache2ctl -t
```

## Configuración del ServerName

Si al probar la configuración de Apache obtenemos el error `Could not reliably
determine the server's fully qualified domain name` , debemos ejecutar el
siguiente comando, para agregar la directiva «SeverName» al archivo de
configuración general de Apache

```bash
echo "ServerName webserver" | sudo tee -a
/etc/apache2/apache2.conf
```

## Habilitar sitio

Si ya no aparece el error volviendo a ejecutar el comando ` apache2ctl -t` procedemos a habilitar el sitio y reiniciar Apache con los siguientes comandos:

```bash
sudo apache2ctl -t
sudo a2ensite jorge.isw811.xyz.conf
sudo systemctl restart apache2.service
```

## Validar sitio

Para visualizar el nuevo sitio desde la máquina anfitriona visitamos la URL
http://jorge.isw811.xyz (o el nombre de dominio que corresponda)

![Sitio Funcionando](./images/Captura%20de%20pantalla%202023-09-27%20012021.png "LAMP")

