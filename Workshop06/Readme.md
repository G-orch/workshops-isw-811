# Workshop 06 Repaso SQL con Northwind

Una base de datos sql utiliza el lenguaje estructurado de consutlas. Se basa en el modelo de base de datos relacionales, donde los datos se  organizan en una o más tablas

## Data Contrtrolling Language (DCL)

Sentencias que se utilizan para controlar el acceso a la base de datos

- Grant
- Revoke

## Data Definition Language (DDL)

comandos SQL que se utilizan para crear, modificar y eliminar laestructura de una base de datos

- Create
- Alter
- Drop
- Rename
- Trunkate
- Comment

### Transaccionalidad:
(hacer rollback/ llevar una bitácora)

### Concurrencia :
Es uno de los problemas que suelen resolver las bases de datos ejemplo: primer comentario

### Integridad referencial: 
Un registro se relaciona con otro y debe de existir/ser válido/ser único


## Transaction Control Language (TCL)

 Sentencias utilizadas para gestionar las transacciones dentro de la base de datos, como la confirmación de una transacción y la anulación de una transacción

 - Set tansaction
 - Savepoint
 - Commit
 - Rollback


## Data manipulation language (DML)

Sentencias SQL que permiten la modificación de los datos que están almacenados en la base de datos

- Insert
- Update
- Delete
- Merge
- Call
- Explain plan
- Lock plan

## Data Query Language

Sentencia utilizada para realizar consultas en la base de datos para recuperar datos

- Select

## Parte Práctica

Descargammos los respaldos de la base de datos y creamos las carpetas dentro de database en la máquina virtual para adjuntar los archivos descargados.

### Ingresamos con ssh:

```bash
vagrant up
vagrant ssh
```

### Ingresamos a :

```bash
vagrant@database:/vagrant/Northwind/RespaldosSQL$ sudo mysql`
```

### Vemos la información:

```bash
MariaDB [(none)]> show data
```

### Creamos un usuario:   

```bash
create user north identified by 'secret';
```

### Creamos la base de datos:

```bash
create database northwind;
```

### Otorgamos privilegios: 

```bash
grant all privileges on northwind.* to north;
```

### Terminar de otorgar privilegios:

```bash
flush privileges;
quit
```

### Para usar BD:

```bash
use northwind
```

### Restauramos la BD `northwind` de los archivos de back-up almacenados en la misma ubicación del Vagrantfile

```bash
$ mysql northwind < northwind.sql -u root -p
$ mysql northwind < northwind-data.sql -u root -p
```

### Para ver usuarios:

```bash
select user from mysql.user;
select user, host from mysql.user;
```


### Conectarse a la BD desde : vagrant@database:/vagrant/Northwind/RespaldosSQL$

```bash
mysql -u north -p
```

Password: secret

Ver las BDs:

```bash
show databases;
```

Conectarse a la BD:

```bash
use northwind;
```

Ver las tablas:

```bash
show tables;
```

Ver tabla específica:

```bash
desc customers;
```

Ver la descripción de la tabla

```sql
SELECT * FROM northwind.inventory_transaction_types;
desc northwind.inventory_transaction_types;
```



# Chanllenges:

Para usar BD en hoja de consultas:  `use northwind;`

1-

```sql
select id as 'codigo' , type_name as 'Description' from inventory_transaction_types
```

![Imagen](./images/ch1.png)

2-
```sql
select  concat(e.first_name, ' ',
e.last_name) as 'Vendedor',
count(1) as 'Cantidad'
from orders o 
join employees e 
on o.employee_id = e.id
group by e.first_name
order by e.id asc;
```

![Imagen](./images/ch2.png)

3-
```sql
select p.product_code as 'Código', 
p.product_name as 'Producto',
round(sum(od.quantity), 2) as 'Cantidad'
from products p 
join order_details od
on od.product_id = p.id 
group by 1, 2
order by 3 desc;

select
ROW_NUMBER() OVER (ORDER BY sum(od.quantity) DESC) AS '#',
p.product_code as 'Código', 
p.product_name as 'Producto',
round(sum(od.quantity), 2) as 'Cantidad'
from products p 
join order_details od
on od.product_id = p.id 
group by 2, 3
order by 4 desc;

```

![Imagen](./images/ch3.png)

4-
```sql
select count(1) as 'Cantidad',
concat(e.first_name, ' ' , e.last_name) as 'Nombre vendedor',
round(sum(od.quantity * od.unit_price), 2) as 'Monto facturado'
from invoices i
join orders o
on o.id = i.order_id
join order_details od
on od.order_id = o.id
join employees e
on e.id = o.employee_id
where o.status_id <> 0
and od.status_id in (2, 3) 
group by e.id
order by 1 desc, 2 asc;

```

![Imagen](./images/ch4.png)

5-

```sql
select p.product_code as 'Código',
p.product_name as 'Producto',
i.quantity as 'Cantidad'
from northwind.inventory_transactions i
join products p 
on i.product_id = p.id
where i.transaction_type = 1
group by i.product_id;
```

![Imagen](./images/ch5.png)

6-

```sql
select p.product_code as 'Código',
p.product_name as 'Producto',
sum(i.quantity) as 'Cantidad'
from northwind.inventory_transactions i
join products p 
on i.product_id = p.id
where i.transaction_type <> 1
group by i.product_id;
```

![Imagen](./images/ch6.png)

7-

```sql
select p.product_code as 'Código',
p.product_name as 'Producto',
t.type_name as 'Tipo',
DATE_FORMAT(i.transaction_created_date, '%d/%m/%Y') as 'Fecha',
i.quantity as 'Cantidad'
from inventory_transactions i
join products p 
on i.product_id = p.id
join inventory_transaction_types t
on i.transaction_type = t.id
where i.transaction_created_date BETWEEN '2006-03-22' AND '2006-03-24'
order by p.product_name asc;
```

![Imagen](./images/ch7.png)

8-

```sql
select p.product_code as 'Código',
p.product_name as 'Producto',
sum(case when t.id = 1 then i.quantity else 0 end) AS Ingreso,
sum(case when t.id <> 1 then i.quantity else 0 end) as Salida,
sum(case when t.id = 1 then i.quantity else -i.quantity end) as Disponible
from inventory_transactions i
join products p 
on i.product_id = p.id
join inventory_transaction_types t
on i.transaction_type = t.id
group by i.product_id;
```

![Imagen](./images/ch8.png)
