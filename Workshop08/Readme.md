# Workshop 08 Soluciones de computo en nube (Mini Hackaton David/Jorge)

Capacidad de cómputo a nivel local, poder tenerlo en la nube, se puede seleccionar diferntes opcines de configuración.

- Hosting (HostGator, GoDaddy)
- Servidores
- Almacenamiento (Dropbox, Drive, One Drive)

## Iaas Infraestructure as a service

- Modelo de servicio de computación en la nube
- Aloja servicios en una nube pública
- Primer gran proveedor fue Amazon
  * Se deben de hacer redes, subredes, se debe de tener conocimiento de infraestructura etc (virtualización).

## PaaS Plataform as a service

- Permite a los desarrolladores construir, implementar y gestionar apolicaciones sin la complejidad asociada a la construcción y mantenimiento de la infraestructura subyacente.
- Elimina la necesidad de preocuparse por aspectos como el hardware y almacenamiento etc...

* No se debe de tener conocimiento de infraestructura, se puede desplegar aplicación sin preocuparse por versiones etc (contenerización).

## SaaS Software as a service

- Modelo de licenciamiento y distribución de software
- A través de un navegador web
  
  * A diferencia de software de la modalidad retail (perpetua- pertenece al usuario de por vida) Inicialmente el software se actualizaba muy poco por lo que se adjuntó el licenciamiento software asurance lo cual permite actualizaciones de las versiones de windows mientras esté pagando anualmente que sería el 22% del precio original; por otra parte, con SaaS el software nunca es propiedad del cliente es un tipo de subscripción que estaría pagando mensual/anualmente. La mayoría de software libre se comercializa de esta manera.

## Plataformas Paas (viedo):

- Railway
- Render.com
- Vercel
- Fly.io
- Northflank

# Pasos para desplegar aplicación Laravel en PaaS

## Base de datos

Se utilizará la plataforma Railway donde inicialmente crearemos un nuevo proyecto en el botón `Start a new Project` lo cual nos mostrará la siguente pantalla donde nos solicita la base de datos a tilizar

![Imagen](./images/2.jpg)


![Imagen](./images/1.jpg)


## En Aplicación

En el proyecto local debemos realizar unas configuraciones para poder hacer uso de Vercel

1- En la raiz del proyecto se procede a crear una carpeta con el nombre de `api` y dentro de este creamos un archivo `index.php`  dentro del cual  insertaremos el siguiente código:

```bash
    <?php

  // Forward Vercel requests to normal index.php
  require __DIR__ . '/../public/index.php';
```

2- Seguidamente, creamos un archivo llamado `.vercelignore` en la raíz del proyecto el cual tendrá la sigueinte línea:

```bash
  /vendor
```

3- Aún posicionados en la carpeta raíz, procedemos a crear un archivo `vercel.json` en el que agregamos la siguiente configuración con las variables de entorno respectivas del proyecto:

```bash
  {
  "version": 2,
    "framework": null,
    "functions": {
      "api/index.php": { "runtime": "vercel-php@0.6.0" }
  },
  "routes": [{
      "src": "/(.*)",
      "dest": "/api/index.php"
  }],
  "env": {
      "APP_ENV": "production",
      "APP_DEBUG": "true",
      "APP_NAME":"Laravel",
      "APP_KEY":"YOURAPPKEYHERE",

      "DB_CONNECTION":"mysql",
      "DB_HOST":"YOURDBHOSTHERE",
      "DB_PORT":"YOURDBPORTHERE",
      "DB_DATABASE":"YOURDBDATABASETHERE",
      "DB_USERNAME":"YOURDBUSERNAMEHERE",
      "DB_PASSWORD":"YOURDBPASSWORDHERE",

      "APP_CONFIG_CACHE": "/tmp/config.php",
      "APP_EVENTS_CACHE": "/tmp/events.php",
      "APP_PACKAGES_CACHE": "/tmp/packages.php",
      "APP_ROUTES_CACHE": "/tmp/routes.php",
      "APP_SERVICES_CACHE": "/tmp/services.php",
      "VIEW_COMPILED_PATH": "/tmp",

      "CACHE_DRIVER": "array",
      "LOG_CHANNEL": "stderr",
      "SESSION_DRIVER": "cookie"
  }
}
```

## Importación del repositorio del proyecto Laravel y su despliegue en Vercel

Vercel facilita la implementación de proyectos vinculados a la cuenta con la que iniciamos sesión. En consecuencia, es necesario cargar el proyecto junto con todas las configuraciones previamente realizadas en el repositorio Git correspondiente.

Procedemos a publicar la aplicación Laravel en Vercel donde podemos iniciar cuenta en los diferentes repositorios git donde tenemos nuestro poyecto; una vez realizado esto, nos ubicaremos en el dashboard de Vercel donde procedemos a agregar nuestro proyecto

![Imagen](./images/3.jpg)

Seguidamente se desplegara un panel donde procedemos a buscar nuestro proyecto e importarlo

![Imagen](./images/4.jpg)

Procedemos a realizar las siguientes configuraciones en la opción `Build and output settings` y finalmente podemos presionar el botón de Deply donde se empezará a configurar automaticamente


![Imagen](./images/5.jpg)

Una vez desplegado nos mostrará un mensaje de felicitaciones indicando que el despliegue ha sido exitoso

Nuestra página desplegada en Vercel: [Mini-Hackathon-2](https://mini-hackathon-2.vercel.app/)

![Imagen](./images/6.jpg)


![Imagen](./images/7.jpg)

## Prueba de conexión Front y Back

- Signup

![Imagen](./images/9.jpg)

- Login

![Imagen](./images/10.jpg)

- Home

![Imagen](./images/11.jpg)

- Database

![Imagen](./images/8.jpg)