# Workshop03 - Laravel con VHOSTs en Apache

## Editamos el archivo hosts en la máquina anfitriona en:

```bash
cd Windows\System32\drivers\etc
notepad hosts
```
Agregamos las entradas al archivo:

Nuevos sitios a hospedar en el Apache2:

```bash
192.168.56.10 jorge.isw811.xyz
192.168.56.10 lfts.isw811.xyz
192.168.56.10 lospatitos.com
192.168.56.10 elblogdejorge.com
```

Procedemos a probar los sitios mediante ping en la máquina virtual

![Ping de sitios](./images/Captura%20de%20pantalla%202023-10-03%20232003.png)


## Seguidamente trabajamos en la configuración de cada sitio

Creamos copias del archivo que ya se había creado en la clase anterior:

![Copias](./images/Captura%20de%20pantalla%202023-10-03%20200516.png)


## Luego reemplazamos las entradas `jorge` por las que correspondan al dominio que queremos hospedar.

```bash
sed -i -e 's/jorge/lfts/g' lfts.isw811.xyz.conf

sed -i -e 's/jorge\.isw811\.xyz/lospatitos\.com/g' lospatitos.com.conf

sed -i -e 's/jorge\.isw811\.xyz/elblogdejorge\.com/g' elblogdejorge.com.conf
```

O procedemos a editar los archivos directamente en VSC

Podemos verificar los cambios con el comando cat:  

```bash
cat lfts.isw811.xyz.conf
cat lospatitos.com.conf
cat elblogdejorge.com.conf
```

## Creamos directorios para cada sitio 

En el directorio padre `sites` creamos un html dentro de cada directorio creado

![Directorios](./images/Captura%20de%20pantalla%202023-10-03%20234341.png)

Procedemos a modificar el html para ver los cambios aplicados en cada sitio

![Html](./images/patos.png)

## Copiar los archivos de configuración de los VHOSTS

mediante los siguientes comandos:

```bash
vagrant ssh
sudo cp elblogdejorge.com.conf /etc/apache2/sites-available/
sudo cp lfts.isw811.xyz.conf /etc/apache2/sites-available/
sudo cp lospatitos.com.conf /etc/apache2/sites-available/

#O todos con un solo comando desde cualquier ubicación por que lleva ruta absoluta
sudo cp /vagrant/confs/* /etc/apache2/sites-available/
```

## Habilitar sitios creados

Mediante los siguientes comandos procedemos a habilitar los sitios:

```bash
sudo a2ensite lfts.isw.811.xyz.conf
sudo a2ensite lospatitos.com.conf
sudo a2ensite elblogdejorge.com.conf

#verificación de errores
sudo apache2ctl -t
```

## Reinicio de apache

Con el siguiente comando reiniciamos apache para poder ver los sitios:

```bash
sudo systemctl reload apache2
```
 ## Comprobamos que la configuracion sirva

 Abrimos los sitios para corroborar.

![Html](./images/Sitios.png)

## Creando proyecto Laravel

Una vez creados los sitios procedemos a correr los siguientes comandos para crear el proyecto Laravel

```bash
#Se descarga instalador de Composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

#Con php se genera el binario de Composer
php composer-setup.php

#Se elimina el archivo descargado
rm composer-setup.php

#Creamos carpeta a /opt/composer y movemos binario a esta
sudo mkdir -p /opt/composer/
sudo mv composer.phar /opt/composer/

#Creamos enlace simbolico para poder ejecutar Composer en la carpeta /usr/bin
sudo ln -s /opt/composer/composer.phar /usr/bin/composer

#Nos dirigimos al directorio de sitios para crear plantilla de laravel
cd /vagrant/sites

#Se elimina proyecto dummy
rm -r lfts.isw811.xyz

#Creamos nuevo proyecto con Composer
composer create-project laravel/laravel:8.6.12 lfts.isw811.xyz
```

Seguidamente debemos modificar el archivo VHOST `lfts.isw811.conf` donde en las lineas 6 y 8 le agregamos la palabra `/public` de la siguiente manera:

![Html](./images/public.png)

Finalmente con estas configuraciones, guardamos lo editado y luego copiamos los archivos en la máquina virtual en la siguiente ruta y con los siguientes comandos:

```bash
/vagrant/confs
sudo cp lfts.isw811.xyz.conf /etc/apache2/sites-available/

# Reiniciamos apache
sudo systemctl reload apache2
```
Ahora si el sitio Laravel debería poder visualizarse

![Html](./images/LARAVEL.png)
