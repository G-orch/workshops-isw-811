# Workshop04 Aprovisionar Servidor de Base de Datos

Primero procedemos a brindarle más memoria a VirtualBox en el Vagrantfile para soportar ambos servidores:

```bash
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
 end
 ```

![Aumento memoria](./images/Aumento%20de%20memoria%20Virtual.png)

# Aprovisionamiento de la base de datos en nueva máquina virtual

Crear una nueva carpeta en el directorio de «VMs». En este caso, como la función de la máquina será servidor de base datos, podemos llamar el folder "database" o "mariadb".

En la máquina anfitriona en la sig ruta:  

```bash
C:\cygwin64\home\Jorge\ISW811\VMs
```
Creamos la carpeta e inicializamos...

```bash
mkdir database
cd database
vagrant init debian/bookworm64
```
Modificamos la ip de la nueva máquina virtual en el Vagrantfile abriendo el archivo con code:

```bash
 ISW811 > VMs > database

 code Vagrantfile
```
![Ip database](./images/ipDataBase.png)

Inicializamos la nueva máquina mediante el comando `vagrant up` en la carpeta creada:

```bash
C:\cygwin64\home\Jorge\ISW811\VMs\database
```
Una vez inicializada ingresamos a esta mediante el comando `vagrant ssh`

## Cambiar nombre a máquina

Para cambiar el nombre a un host tipo Unix, lanzamos el comando hostnamectl y luego editamos el archivo /etc/hosts, editando la línea 127.0.0.2 bookworm por 127.0.0.2 database.

```bash
sudo hostnamectl set-hostname database
sudo nano /etc/hosts
```
 El archivo lo podemos editar con sudo nano /etc/hosts, guardamos con [Ctrl+O] enter y salimos del editor «nano» con [Ctrl+X].

```bash
127.0.0.1 localhost
127.0.0.2 database
::1 localhost ip6-localhost ip6-loopback
```

apagamos la máquina `exit`, `vagrant halt` y volvemos a ingresar (database) `vagrant ssh` para ver el cambio

## Instalar paquetes de MariaDB

```bash
sudo apt-get update
sudo apt-get install mariadb-server mariadb-client
```
Eliminar funciones de servidor de base de datos en la máquina `webserver` conectandonos mediante `vagrant up` y `vagrant ssh`

```bash
sudo apt-get remove mariadb-server
```
  ** OJO, SE EJECUTAN EN EL SERVIDOR "webserver" 

  ## Crear usuario y BD en el servidor "database" mediante ssh

```bash
sudo mysql
MariaDB [(none)]> create user laravel identified by 'secret';
MariaDB [(none)]> create database lfts;
MariaDB [(none)]> grant all privileges on lfts.* to laravel;
MariaDB [(none)]> flush privileges;
MariaDB [(none)]> quit
```

Comprobamos credenciales y acceso mediante:

```bash
mysql -u laravel -p
MariaDB [(none)]> show databases;
```

Debería mostrar algo como lo siguiente:

```bash
+--------------------+
| Database |
+--------------------+
| information_schema |
| lfts |
+--------------------+
```

## Habilitar acceso remoto

Para habilitar el acceso remoto a la base de datos «MariaDB» desde cualquier equipo de la red,vamos a descomentar la directiva bind-address = 127.0.0.1, del archivo /etc/mysql/mariadb.conf.d/50-server.cnf, el cual podemos editar con «nano», con el siguiente comando

```bash
sudo nano -l /etc/mysql/mariadb.conf.d/50-server.cnf
```
Una vez dentro del archivo simplemente documentamos la línea con # y guardamos `Ctrl+x enter` `Ctrl+x`

![Acceso remoto BD](./images/AccRemoto.png)

Comprobamos el acceso remoto conectandonos al servidor `webserver` y desde ahí lo intentamos con:

```bash
mysql -h 192.168.56.11 -u laravel -p
```

```bash
MariaDB [(none)]> show databases;
+--------------------+
| Database |
+--------------------+
| information_schema |
| lfts |
+--------------------+
```

--------------------------------------------------

## Instalar NVM (Node Version Manager) en Webserver

### Instalamos un administrador de versiones para node.js, diseñado para instalarse por usuario e invocarse por shell.

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
```

Podemos ver las versiones disponibles de node con el siguiente comando:

```bash
nvm ls-remote
```

## Instalar la versión Hydrogen que es compatible con Laravel 6.8.12

```bash
nvm install --lts=hydrogen
```

## Instalar Laravel UI con la ayuda de Composer y node.js con los siguientes comandos:

```bash
cd /vagrant/sites/lfts.isw811.xyz

composer require laravel/ui:3.4.6

php artisan ui bootstrap

npm install && npm run dev

npm install && npm run dev  #comando ejecutado 2 veces
```

En este punto me salió un error que hay que corregir:

![Error](./images/Error.jpg)

## Finalmente creamos las vistas de autenticación utilizando Bootstrap como framework.

Este paso no se ha realizado, solo modificado el archivo .env...

```bash
php artisan ui bootstrap --auth

npm install && npm run dev
```

## Configuración de parámetros de la BD

Se edita el archivo .env desde la máquina anfitriona ubicado en la sig dirección: 

```bash
ISW811 > VMs > webserver > sites > lfts.isw811.xyz > .env
```

quedaría de la siguiente forma:

![Archivo .env](./images/Archivo%20.env.png)


## Inicializar la BD

Ejecutamos las migraciones del proyecto

```bash
cd /vagrant/sites/lfts.isw811.xyz
$ php artisan migrate
```

Una vez configurado todo salimos de las maquinas virtuales con 

```bash
exit
```

Y seguidamente apagamos las máquinas virtuales con el comando `vagrant halt` 

## Comando para crear archivo .tar.gz para los entregables de LFTS

```bash
cd ~/ISW811/VMs/webserver/sites

tar cvfz lfts.isw811.xyz.tar.gz --exclude={'lfts.isw811.xyz/vendor','lfts.isw811.xyz/node_modules'} lfts.isw811.xyz

```
