# Workshop 07 - Dot files Bash scripting, Cronjobs

Un script es un programa con sus instrucciones, bucles, variables, automatizar tareas.

## Mi primer Script

Creamos el folder y el archivo `.sh` que contendrá nuestro primer programa

En el archivo agregamos la siguiente sentencia:

```bash
#!/bin/bash
echo "Hello world"
```

![Imagen](./images/1.png)

Nos conectamos a la máquina virtual permiso de ejecución:
```bash
vagrant ssh
cd /vagrant/Scripts
chmod +x hello.sh 
./hello.sh
```

Por seguridad:

Se debe de invocar desde la ruta exacta en vagrant/Scripts ó:


Si deseo hacerlo accesible desde cualquier ubicacion se debe de mover a cualquier directorio desde la ruta opt

```bash
sudo mv /vagrant/Scripts/hello.sh /opt/scripts/
```

Variable:

echo $PATH

agregar al path ruta opt/script

Con este comando no se guarda la variable de entorno:
```bash
vagrant@database:~$ export PATH=$PATH:/opt/scripts
```

## Dotfiles

## Estrategia 1:Agregar la ruta al PATH

Para manejar configuraciones o scripts de arranque editamos el archivo para declarar la variable PATH en ruta raiz de máquina virtual

```bash
nano .bashrc
#al final del archivo:
export PATH=$PATH:/opt/scripts
```

Ctrl O enter para guardar
Ctrl X salir del editor de texto

Logramos que el comando `hello.sh` funcione desde cualquier ubicación; podemos salir y volver a ingresar a la máquina y veremos como sirvieron los cambios

Creamos un Script nuevo  `helloworld.sh` con las siguientes funciones:

```bash
#!/bin/bash

echo "Hello world"

hola () {
    echo "Hola Mundo!"
}

hello () {
    echo "Hello world!"
}

if [ -z $1]; then
echo "debe de pasar el argumento del idioma"
exit o
fi

case $1 in 
"spanish")
    hola
;;
"english")
    hello
;;
*)
   echo "Solo se admiten los parametros spanish o english"
;;
esac
```

![Imagen](./images/2.png)


Damos permisos al script:

```bash
chmod +x helloworld.sh
```

Podemos ejecutar las funciones: 

```bash
vagrant@database:/vagrant/Scripts$ ./helloworld.sh spanish
vagrant@database:/vagrant/Scripts$ ./helloworld.sh english
```

## Estrategia 2: Colocar el binario en una ubicación del PATH

Hacer que el binario sea accesible através de una de las rutas que ya forman parte del PATH

Para este ejemplo creamos una segunda versión del script

Luego desde la máquina virtual lo hacemos ejecutable y lo movemos a `/opt/scripts`.

```bash
vagrant ssh
cd /vagrant/Scripts
chmod +x helloworld.sh
sudo cp /vagrant/Scripts/helloworld.sh /opt/scripts
```

## Back up de la BD:

```bash
mysqldump northwind > norhwind-backup.sql -u north -p
```

Para utilizar Cron:

```bash
crontab -e
```

Enlace simbólico:

```bash
sudo ln -s /opt/scripts/db-backup.sh /usr/local/bin/db-backup
```

## Archivo auto-deployer:
Para desplegar los procesos automaticamente

```bash
#!/bin/bash

echo "Autodeployer v0.1"

deployer () {
    echo "Desplegando: "$1
    chmod +x $1
    sudo mkdir -p /opt/scripts
    sudo cp $1 /opt/scripts 
    link=$(basename $1 .sh)
    echo "Link: ["$link"]"
    sudo ln -s /opt/scripts/$1 /usr/local/bin/$link
}

for i in *.sh; do deployer($i); done
```

## Archivo db.backup:

Con este archivo creamos automaticamente respaldos de la BD

```bash
#!/bin/bash
if [ -z "$1"]; then
    echo "Debe pasar la ruta del archivo.env"
    exit 1
else
    source "$1"
fi

datatime=$(date '+%Y%m%d_%H%M%S')

if [ -z $1]; then
    filename=$filename"_"$datatime.sql
else
    trimname=$(echo "$1"  | tr -d " ")
    filename=$filename"_"$trimname.sql
fi

echo "Iniciando respaldo..." 
mysqldump $database > $backups/$filename -u $user --password=$password

echo "Comprimiendo el respaldo..."
cd $backups
tar cvfz $filename.tar.gz $filename
rm $filename
```

## Archivo .env-northwind:

Archivo para almacenar variables de entorno

```bash
#!/bin/bash

backups=/home/vagrant/backups
database=northwind
filename=northwind
user=north
password=secret
datatime=$(date '+%Y%m%d_%H%M%S')
```
